package bsa.java.concurrency.image.service;

import bsa.java.concurrency.Configuration.Settings;
import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.image.ImageRepository;
import bsa.java.concurrency.image.Model.Image;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;


@Service
public class ImageService {
    @Autowired
    private Settings settings;
    @Autowired
    private FileSystemService fileSystem;
    @Autowired
    private DHasher hasher;
    @Autowired
    private ImageRepository imageRepository;

    public void multiDownload(Map<byte[], String> files) {
        List<CompletableFuture<Void>> results = new ArrayList<>();
        files.entrySet().stream().parallel().forEach(x-> results.add(downloadFileAndSave(x.getKey(),x.getValue(), null)));
        CompletableFuture.allOf(results.toArray(new CompletableFuture [results.size()])).join();
    }


    public CompletableFuture<Void> downloadFileAndSave(byte[] file, String name, Long hash) {
        CompletableFuture<String> futurePath = fileSystem.saveFile(file, name);
        Long myHash = hash == null ? hasher.calculateHash(file) : hash;
        String url = "http://localhost:" + settings.getPort() + "/" + "files"
                + "/" + new File(settings.getPROJECT_FOLDER()).getName()  + "/" + name;
        return futurePath.thenAccept(path->imageRepository.save(Image.builder().Hash(myHash).URL(url).build()));
    }

    public void deleteAll() {
        imageRepository.deleteAll();
        File folder = new File(settings.getPROJECT_FOLDER());
        if (!folder.exists()) return;
        for (File f : folder.listFiles()) {
            f.delete();
        }
    }

    public void deleteById(UUID id) {
        Optional<Image> name = imageRepository.findById(id);
        if (name.isEmpty()) return;
        String path = name.get().getURL();
        path = path.substring(path.lastIndexOf("/"));
        File file = new File(settings.getPROJECT_FOLDER() + path);
        file.delete();
        imageRepository.deleteById(id);
    }

    public List<SearchResultDTO> findMatches(byte[] file, String name, Double threshold) {
        Long hash = hasher.calculateHash(file);
        List<SearchResultDTO> result = imageRepository.getMatches(threshold, hash);
        if (result.size()==0) downloadFileAndSave(file, name, hash);
        return result;
    }
}
