package bsa.java.concurrency.image;

import bsa.java.concurrency.Configuration.Settings;
import bsa.java.concurrency.Exception.NoValidRequestException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/image")
public class ImageController {
    @Autowired
    private ImageService imageService;
    @Autowired
    private Settings settings;

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) throws IOException {
        if (files.length==0) throw new NoValidRequestException("content to upload not found");
        if (files.length > settings.getMaxFileCount())
            throw new NoValidRequestException("too many files, max available count is " + settings.getMaxFileCount());
        Map<byte[], String> filelist = new HashMap<>();
        for(MultipartFile file: files) {
            if (file.getSize()>settings.getMaxFileSize())
                throw new NoValidRequestException(file.getOriginalFilename() + " - maximal size of file exceeded");
            filelist.put(file.getBytes(), file.getOriginalFilename());
        }
        imageService.multiDownload(filelist);
        System.out.println("saved");
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold)
                                                throws IOException
    {
        if (threshold>1 || threshold<0) throw new NoValidRequestException("threshold should be in [0..1] interval");
        if (file.getSize()>settings.getMaxFileSize())
            throw new NoValidRequestException(file.getOriginalFilename() + " - maximal size of file exceeded");
        var res = imageService.findMatches(file.getBytes(), file.getOriginalFilename(), threshold);
        System.out.println("now return result");
        return res;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        if(imageId==null) throw new NoValidRequestException("image id shouldn't be null");
        imageService.deleteById(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        imageService.deleteAll();
    }
}
