package bsa.java.concurrency.image;

import bsa.java.concurrency.image.Model.Image;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(value = "SELECT cast(i.id as varchar) as id, i.image_url as image, (1-(SELECT length(replace(cast(x as text), '0', '')) " +
            "from (values (cast(:newhash as bit(64)) # cast(i.image_hash as bit(64)))) as something(x))/64.) as match " +
            "FROM images i " +
            "WHERE (1-(SELECT length(replace(cast(x as text), '0', '')) " +
            "from (values (cast(:newhash as bit(64)) # cast(i.image_hash as bit(64)))) as something(x))/64.) >= :threshold " +
            "ORDER BY match DESC",
            nativeQuery = true)
    List<SearchResultDTO> getMatches(@Param("threshold")Double threshold, @Param("newhash")Long newhash);
}

/*
@Query(value = "SELECT cast(i.id as varchar) as imageId, i.image_url as imageUrl, (1-(SELECT length(replace(cast(x as text), '0', '')) " +
            "from ( values (cast(:newhash as bit(64)) # cast(i.image_hash as bit(64)))) as something(x))/64) as matchPercent FROM images i " +
            "WHERE (1-(SELECT length(replace(cast(x as text), '0', '')) " +
            "from ( values (cast(:newhash as bit(64)) # cast(i.image_hash as bit(64)))) as something(x))/64) > :threshold",
            nativeQuery = true)*/
