package bsa.java.concurrency.image.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.UUID;

@JsonPropertyOrder({ "id", "image", "match" })
public interface SearchResultDTO {
    UUID getId();
    String getImage();
    Double getMatch();
}
