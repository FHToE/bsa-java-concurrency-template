package bsa.java.concurrency.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "bad request")
public class NoValidRequestException extends RuntimeException{
    public NoValidRequestException(String message) {
        super(message);
    }
}
