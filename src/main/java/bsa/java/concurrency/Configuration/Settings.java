package bsa.java.concurrency.Configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class Settings {
    @Value(value = "${ROOT_FOLDER}")
    private String PROJECT_FOLDER;
    @Value(value = "${server.port}")
    private int Port;
    @Value(value = "${MAX_FILE_SIZE}")
    private int maxFileSize;
    @Value(value = "${MAX_COUNT_OF_FILES}")
    private int maxFileCount;
}
