package bsa.java.concurrency.fs;

import bsa.java.concurrency.Configuration.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

@Service
public class FileSystemService {
    @Autowired
    private Settings settings;
    @Autowired
    private TaskExecutor taskExecutor;

    public CompletableFuture<String> saveFile(byte[] file, String name){
        CompletableFuture<String> res = CompletableFuture.supplyAsync(() -> {
            Path path = Path.of(settings.getPROJECT_FOLDER()).resolve(name);
            File f = new File(path.toString());
            InputStream targetStream = new ByteArrayInputStream(file);
            if (f.exists()) f.delete();
            try{
                System.out.println("Start copy file: " + f);
                Files.copy(targetStream, path);
            } catch (Exception e) {
                System.out.println("Exception with file: " + f +" : " + e.getMessage());
                throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
            }
            System.out.println("Finish copy file: " + f);
            String result = f.getParentFile() + File.separator + f.getName();
            return result;
        }, taskExecutor);
        return res;

    }
}
